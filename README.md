# seir-sr AnyLogic model

## Postup inštalácie

1. stiahnutie repozitára k sebe
2. inštalácia nástroja Anylogic (min version: 8.5.2)
* https://www.anylogic.com/downloads/personal-learning-edition-download/
* **Na bežné experimenty plne postačuje free personal edition**
* Pre ďalšie experimenty ako je napr. kalibrácia, alebo optimalizácie je potrebná vyššia verzia (30 dňový eval. postačuje)

3. po inštalácii otvoriť model v adresári SEIR-ageGroups
4. "bežná" simulácia sa spúšťa cez Run nad Simulation2:Main
5. pre pokročilejšie zmeny parameterov vid. Main trieda

